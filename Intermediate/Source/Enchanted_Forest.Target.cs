using UnrealBuildTool;

public class Enchanted_ForestTarget : TargetRules
{
	public Enchanted_ForestTarget(TargetInfo Target) : base(Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V2;
		Type = TargetType.Game;
		ExtraModuleNames.Add("Enchanted_Forest");
	}
}
